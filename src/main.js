import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
axios.defaults.headers.get['Accepts'] = 'application/json'
axios.defaults.headers['Content-Type'] = 'application/json'
axios.defaults.headers.common['Authorization'] = process.env.VUE_APP_AUTHORIZATION
Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')